FROM node:14.17.5-alpine3.11 AS development

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./

RUN npm install glob rimraf
RUN npm install --only=development

# Bundle app source
COPY . .

RUN npm run build

FROM node:14.17.5-alpine3.11 as production

LABEL MAINTAINER="Infinity Management <rami.safari@infinitymgt.fr>"

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --only=production

COPY . .
COPY --from=development /usr/src/app/dist ./dist

EXPOSE 3000

CMD ["npm", "run", "start:prod" ]

