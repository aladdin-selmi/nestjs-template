db.createUser({
	user: "${DATABASE_ROOT}",
	pwd: "${DATABASE_ROOT_PWD}",
	roles: [
		{
			role: "userAdminAnyDatabase",
			db: "admin",
		},
	],
});
db.createUser({
	user: "${DATABASE_USER}",
	pwd: "${DATABASE_PASSWORD}",
	roles: [
		{
			role: "readWrite",
			db: "${DATABASE_NAME}",
		},
	],
});
