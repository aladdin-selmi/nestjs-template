import { NodePlopAPI } from "plop";
import { moduleActions } from "./src/config/plop/module-actions";
import { moduleInputs } from "./src/config/plop/module-inputs";

module.exports = function (plop: NodePlopAPI) {
	// A module skeleton generator
	plop.setGenerator("module", {
		description: "Module skeleton generator",
		prompts: moduleInputs(plop),
		actions: (answers) => moduleActions(plop, answers),
	});
};
