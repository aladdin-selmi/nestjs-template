import { Controller, Get, Param, Query } from "@nestjs/common";
import { ApiBearerAuth, ApiOperation, ApiTags } from "@nestjs/swagger";
import { AladdinsService } from "./aladdins.service";
import { SearchAladdinDto } from "./dto/search-aladdin.dto";

@ApiTags("Aladdins - App")
@Controller("app-api/aladdins")
export class AladdinsAppController {
	constructor(private readonly aladdinsService: AladdinsService) {}

	@Get()
	@ApiOperation({ summary: "Find all" })
	@ApiBearerAuth()
	findAll(@Query() searchAladdinDto: SearchAladdinDto) {
		return this.aladdinsService.findAll(searchAladdinDto);
	}

	@Get(":id")
	@ApiOperation({ summary: "FindOne" })
	@ApiBearerAuth()
	findOne(@Param("id") id: string) {
		return this.aladdinsService.findOne(id);
	}
}
