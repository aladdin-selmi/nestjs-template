import { Test, TestingModule } from '@nestjs/testing';
import { AladdinsController } from './aladdins.controller';
import { AladdinsService } from './aladdins.service';

describe('AladdinsController', () => {
  let controller: AladdinsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AladdinsController],
      providers: [AladdinsService],
    }).compile();

    controller = module.get<AladdinsController>(AladdinsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
