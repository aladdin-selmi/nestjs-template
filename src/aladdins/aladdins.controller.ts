import {
	Controller,
	Get,
	Post,
	Body,
	Patch,
	Param,
	Delete,
	Query,
	UsePipes,
	ValidationPipe,
} from "@nestjs/common";
import { ApiBearerAuth, ApiOperation, ApiTags } from "@nestjs/swagger";
import { AladdinsService } from "./aladdins.service";
import { CreateAladdinDto } from "./dto/create-aladdin.dto";
import { SearchAladdinDto } from "./dto/search-aladdin.dto";
import { UpdateAladdinDto } from "./dto/update-aladdin.dto";
import { Roles } from "../auth/roles.decorator";
import { UserRolesEnum } from "../users/models/roles.enum";

@ApiTags("Aladdins - Admin")
@Controller("admin-api/aladdins")
@Roles(UserRolesEnum.ADMIN)
@UsePipes(new ValidationPipe({ transform: true, whitelist: true }))
export class AladdinsController {
	constructor(private readonly aladdinsService: AladdinsService) {}

	@Post()
	@ApiOperation({ summary: "Create" })
	@ApiBearerAuth()
	create(@Body() createAladdinDto: CreateAladdinDto) {
		return this.aladdinsService.create(createAladdinDto);
	}

	@Get()
	@ApiOperation({ summary: "Find all" })
	@ApiBearerAuth()
	findAll(@Query() searchAladdinDto: SearchAladdinDto) {
		return this.aladdinsService.findAll(searchAladdinDto);
	}

	@Get(":id")
	@ApiOperation({ summary: "FindOne" })
	@ApiBearerAuth()
	findOne(@Param("id") id: string) {
		return this.aladdinsService.findOne(id);
	}

	@Patch(":id")
	@ApiOperation({ summary: "Update" })
	@ApiBearerAuth()
	update(@Param("id") id: string, @Body() updateAladdinDto: UpdateAladdinDto) {
		return this.aladdinsService.update(id, updateAladdinDto);
	}

	@Delete(":id")
	@ApiOperation({ summary: "Remove" })
	@ApiBearerAuth()
	remove(@Param("id") id: string) {
		return this.aladdinsService.remove(id);
	}
}
