import { Module } from "@nestjs/common";
import { AladdinsService } from "./aladdins.service";
import { AladdinsController } from "./aladdins.controller";
import { MongooseModule } from "@nestjs/mongoose";
import { addPaginationPlugin, addSoftDeletePlugin } from "../config/mongoose";
import { AladdinSchema } from "./models/aladdin.model";
import { AladdinsAppController } from "./aladdins.app.controller";

@Module({
	controllers: [AladdinsController, AladdinsAppController],
	providers: [AladdinsService],
	imports: [
		MongooseModule.forFeatureAsync([
			{
				name: "Aladdin",
				useFactory: () => {
					addSoftDeletePlugin(AladdinSchema);
					addPaginationPlugin(AladdinSchema);
					return AladdinSchema;
				},
			},
		]),
	],
})
export class AladdinsModule {}
