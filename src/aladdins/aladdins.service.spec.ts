import { Test, TestingModule } from '@nestjs/testing';
import { AladdinsService } from './aladdins.service';

describe('AladdinsService', () => {
  let service: AladdinsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AladdinsService],
    }).compile();

    service = module.get<AladdinsService>(AladdinsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
