import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model, Types } from "mongoose";
import { QueryManager } from "../query-manager/query-manager";
import { CreateAladdinDto } from "./dto/create-aladdin.dto";
import { SearchAladdinDto } from "./dto/search-aladdin.dto";
import { UpdateAladdinDto } from "./dto/update-aladdin.dto";
import { Aladdin } from "./models/aladdin.model";

@Injectable()
export class AladdinsService {
	constructor(
		@InjectModel("Aladdin")
		private readonly aladdinModel: Model<Aladdin>,
		private readonly queryManager: QueryManager,
	) {}

	async create(createAladdinDto: CreateAladdinDto) {
		const aladdin = new this.aladdinModel(createAladdinDto);
		return await aladdin.save();
	}

	async findAll(searchAladdinDto: SearchAladdinDto) {
		const filter = this.queryManager.buildFilterQuery(
			this.aladdinModel,
			searchAladdinDto,
			["title"],
		);

		return await this.queryManager.buildPaginationQuery(
			this.aladdinModel,
			searchAladdinDto,
			filter,
		);
	}

	async findOne(id: string) {
		const aladdin = await this.aladdinModel.findById(id);
		if (!aladdin) throw new NotFoundException("Aladdin not found");
		return aladdin;
	}

	async update(id: string, updateAladdinDto: UpdateAladdinDto) {
		const aladdin = await this.aladdinModel.findById(id);
		if (!aladdin) throw new NotFoundException("Aladdin not found");

		return await aladdin.set(updateAladdinDto).save();
	}

	async remove(id: string) {
		// @ts-ignore
		await this.aladdinModel.deleteById(new Types.ObjectId(id));
		return id;
	}
}
