import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";

export class CreateAladdinDto {
	@ApiProperty()
	@IsString()
	title: string;
}
