import { ApiProperty } from "@nestjs/swagger";
import { BaseQueryDto } from "../../query-manager/dto/base-query.dto";

export class SearchAladdinDto extends BaseQueryDto {
	@ApiProperty({ required: false })
	title?: string;
}
