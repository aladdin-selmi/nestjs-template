import { PartialType } from '@nestjs/swagger';
import { CreateAladdinDto } from './create-aladdin.dto';

export class UpdateAladdinDto extends PartialType(CreateAladdinDto) {}
