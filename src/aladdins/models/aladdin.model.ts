import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type AladdinDocument = Aladdin & Document;

@Schema({ timestamps: true })
export class Aladdin {
	@Prop()
	title: string;
}

export const AladdinSchema = SchemaFactory.createForClass(Aladdin);
