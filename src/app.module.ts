import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { MongooseModule } from "@nestjs/mongoose";
import { AladdinsModule } from "./aladdins/aladdins.module";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { AuthModule } from "./auth/admin/auth.module";
import { AuthAppModule } from "./auth/app/auth.app.module";
import { AppConfig } from "./config/app-config";
import { CustomersModule } from "./customers/customers.module";
import { QueryManagerModule } from "./query-manager/query-manager.module";
import { UsersModule } from "./users/users.module";

@Module({
	imports: [
		ConfigModule.forRoot({ isGlobal: true, cache: true, load: [AppConfig] }),
		MongooseModule.forRoot(AppConfig().dbFullString),
		QueryManagerModule,
		UsersModule,
		CustomersModule,
		AuthModule,
		AuthAppModule,
		AladdinsModule,
	],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {}
