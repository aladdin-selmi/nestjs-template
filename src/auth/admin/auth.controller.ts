import {
	Controller,
	Request,
	Post,
	UseGuards,
	Get,
	Body,
} from "@nestjs/common";
import {
	ApiBearerAuth,
	ApiBody,
	ApiExtraModels,
	ApiOperation,
	ApiTags,
	getSchemaPath,
} from "@nestjs/swagger";

import { UsersService } from "../../users/users.service";
import { AuthService } from "./auth.service";
import { LoginDto } from "../dto/login.dto";
import { LocalAuthGuard } from "./local-auth.guard";
import { NoAuth } from "../no-auth.decorator";
import { UpdateUserProfileDto } from "../../users/dto/update-user-profile.dto";

@ApiTags("Auth - Admin")
@ApiExtraModels(LoginDto)
@Controller("admin-api/auth")
export class AuthController {
	constructor(
		private authService: AuthService,
		private usersService: UsersService,
	) {}

	@NoAuth()
	@UseGuards(LocalAuthGuard)
	@Post("login")
	@ApiOperation({ summary: "Login" })
	@ApiBody({
		schema: {
			type: "object",
			$ref: getSchemaPath(LoginDto),
		},
	})
	async login(@Request() req) {
		return this.authService.login(req.user);
	}

	@ApiOperation({ summary: "Get profile" })
	@ApiBearerAuth()
	@Get("profile")
	getProfile(@Request() req) {
		return this.usersService.findOne(req.user.user.id);
	}

	@ApiOperation({ summary: "Update profile" })
	@ApiBearerAuth()
	@Post("profile")
	updateProfile(@Request() req, @Body() updateDto: UpdateUserProfileDto) {
		return this.usersService.updateProfile(req.user.user.id, updateDto);
	}
}
