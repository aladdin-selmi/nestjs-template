import { Module } from "@nestjs/common";
import { PassportModule } from "@nestjs/passport";
import { UsersModule } from "../../users/users.module";
import { AuthService } from "./auth.service";
import { LocalStrategy } from "./local.strategy";
import { AuthController } from "./auth.controller";
import { JwtModule } from "@nestjs/jwt";
import { jwtConstants } from "../../config/jwt";
import { JwtStrategy } from "../jwt.strategy";
import { APP_GUARD } from "@nestjs/core";
import { JwtAuthGuard } from "../jwt-auth.guard";
import { RolesGuard } from "../roles.guard";

@Module({
	providers: [
		AuthService,
		LocalStrategy,
		JwtStrategy,
		{
			provide: APP_GUARD,
			useClass: JwtAuthGuard,
		},
		{
			provide: APP_GUARD,
			useClass: RolesGuard,
		},
	],
	imports: [
		UsersModule,
		PassportModule,
		JwtModule.register({
			secret: jwtConstants.secret,
			signOptions: { expiresIn: "90d" },
		}),
	],
	controllers: [AuthController],
	exports: [AuthService],
})
export class AuthModule {}
