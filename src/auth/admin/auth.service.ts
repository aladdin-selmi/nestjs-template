import { Injectable, NotAcceptableException } from "@nestjs/common";
import { UsersService } from "../../users/users.service";
import * as bcrypt from "bcrypt";
import { JwtService } from "@nestjs/jwt";

@Injectable()
export class AuthService {
	constructor(
		private usersService: UsersService,
		private jwtService: JwtService,
	) {}

	async validateUser(login: string, pass: string): Promise<any> {
		const errorMessage = "Invalid username/password";
		const user = await this.usersService.findByCredentials(login);
		if (!user) throw new NotAcceptableException(errorMessage);

		const checkPassword = await bcrypt.compare(pass, user.password);
		if (!checkPassword) throw new NotAcceptableException(errorMessage);

		return user;
	}

	async login(user: any) {
		const payload = { userId: user._id.toString(), sub: user };
		return {
			access_token: this.jwtService.sign(payload),
			user: user,
		};
	}
}
