import {
	Controller,
	Request,
	Post,
	UseGuards,
	Get,
	Body,
} from "@nestjs/common";
import {
	ApiBearerAuth,
	ApiBody,
	ApiOperation,
	ApiTags,
	getSchemaPath,
} from "@nestjs/swagger";
import { LoginDto } from "../dto/login.dto";
import { NoAuth } from "../no-auth.decorator";
import { CustomersService } from "../../customers/customers.service";
import { AuthAppService } from "./auth.app.service";
import { AppLocalAuthGuard } from "./local-auth.app.guard";
import { RegisterDto } from "../dto/register.dto";
import { UpdateCustomerProfileDto } from "../../customers/dto/update-customer-profile";

@ApiTags("Auth - App")
@Controller("app-api/auth")
export class AuthAppController {
	constructor(
		private authAppService: AuthAppService,
		private customersService: CustomersService,
	) {}

	@NoAuth()
	@Post("register")
	@ApiOperation({ summary: "Register" })
	@ApiBody({
		schema: { type: "object", $ref: getSchemaPath(RegisterDto) },
	})
	async register(registerDto: RegisterDto) {
		return this.customersService.register(registerDto);
	}

	@NoAuth()
	@UseGuards(AppLocalAuthGuard)
	@Post("login")
	@ApiOperation({ summary: "Login" })
	@ApiBody({
		schema: { type: "object", $ref: getSchemaPath(LoginDto) },
	})
	async login(@Request() req) {
		return this.authAppService.login(req.user);
	}

	@ApiOperation({ summary: "Get profile" })
	@ApiBearerAuth()
	@Get("profile")
	getProfile(@Request() req) {
		return this.customersService.findOne(req.user.user.id);
	}

	@ApiOperation({ summary: "Update profile" })
	@ApiBearerAuth()
	@Post("profile")
	updateProfile(@Request() req, @Body() updateDto: UpdateCustomerProfileDto) {
		return this.customersService.updateProfile(req.user.user.id, updateDto);
	}
}
