import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { AuthModule } from "../admin/auth.module";
import { jwtConstants } from "../../config/jwt";
import { CustomersModule } from "../../customers/customers.module";
import { AuthAppService } from "./auth.app.service";
import { AuthAppController } from "./auth.app.controller";
import { AppLocalStrategy } from "./local.app.strategy";

@Module({
	controllers: [AuthAppController],
	providers: [AuthAppService, AppLocalStrategy],
	imports: [
		AuthModule,
		CustomersModule,
		JwtModule.register({
			secret: jwtConstants.secret,
			signOptions: { expiresIn: "90d" },
		}),
	],
})
export class AuthAppModule {}
