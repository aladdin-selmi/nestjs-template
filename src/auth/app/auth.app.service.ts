import { Injectable, NotAcceptableException } from "@nestjs/common";
import * as bcrypt from "bcrypt";
import { JwtService } from "@nestjs/jwt";
import { CustomersService } from "../../customers/customers.service";

@Injectable()
export class AuthAppService {
	constructor(
		private customersService: CustomersService,
		private jwtService: JwtService,
	) {}

	async validateCustomer(login: string, pass: string): Promise<any> {
		const errorMessage = "Invalid username/password";
		const customer = await this.customersService.findByCredentials(login);
		if (!customer) throw new NotAcceptableException(errorMessage);

		const checkPassword = await bcrypt.compare(pass, customer.password);
		if (!checkPassword) throw new NotAcceptableException(errorMessage);

		return customer;
	}

	async login(user: any) {
		const payload = { userId: user._id.toString(), sub: user };
		return {
			access_token: this.jwtService.sign(payload),
			user: user,
		};
	}
}
