import { Strategy } from "passport-local";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { AuthAppService } from "./auth.app.service";

@Injectable()
export class AppLocalStrategy extends PassportStrategy(Strategy, "app-local") {
	constructor(private authAppService: AuthAppService) {
		super();
	}

	/**
	 * Add customer data to Req. To be used when logging in.
	 * @param payload Data after jwt validation
	 * @returns payload to add to Req
	 */
	async validate(username: string, password: string): Promise<any> {
		const customer = await this.authAppService.validateCustomer(
			username,
			password,
		);
		if (!customer) {
			throw new UnauthorizedException();
		}
		return customer;
	}
}
