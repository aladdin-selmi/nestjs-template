import { ExecutionContext, Injectable } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { AuthGuard } from "@nestjs/passport";
import { NO_AUTH_KEY } from "./no-auth.decorator";

@Injectable()
export class JwtAuthGuard extends AuthGuard("jwt") {
	constructor(private reflector: Reflector) {
		super();
	}

	canActivate(context: ExecutionContext) {
		const noAuth = this.reflector.getAllAndOverride<boolean>(NO_AUTH_KEY, [
			context.getHandler(),
			context.getClass(),
		]);
		if (noAuth) return true;
		return super.canActivate(context);
	}

	// handleRequest(err, user, info) {
	// 	// You can throw an exception based on either "info" or "err" arguments
	// 	console.log("err", err);
	// 	console.log("user", user);
	// 	console.log("info", info);
	// 	return user;
	// }
}
