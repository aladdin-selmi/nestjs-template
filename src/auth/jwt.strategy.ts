import { ExtractJwt, Strategy } from "passport-jwt";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable } from "@nestjs/common";
import { jwtConstants } from "../config/jwt";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor() {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			ignoreExpiration: false,
			secretOrKey: jwtConstants.secret,
		});
	}

	/**
	 * Called after successful credentials validation.
	 * Add user data to Req.
	 * @param payload Data after jwt validation
	 * @returns payload to add to Req
	 */
	async validate(payload: any) {
		return { user: payload.sub };
	}
}
