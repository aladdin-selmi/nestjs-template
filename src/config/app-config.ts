export const AppConfig = () => {
	const config = {
		port: parseInt(process.env.PORT, 10) || 3000,

		adminUrl: process.env.ADMIN_URL || "http://mm-admin.infinitymgt.fr",
		appUrl: process.env.FRONT_URL || "http://mm.infinitymgt.fr",

		isProd: process.env.NODE_ENV === "production",
		isDev: process.env.NODE_ENV !== "production",

		dbConnector: process.env.DATABASE_CONNECTOR || "mongodb",
		dbAddress: process.env.DATABASE_ADRESS || "localhost",
		dbPort: parseInt(process.env.DATABASE_PORT, 10) || 27017,
		dbName: process.env.DATABASE_NAME,
		dbAuthSource: process.env.DATABASE_AUTHSOURCE || "admin",

		dbUser: process.env.DATABASE_USER || "infinityCamelion",
		dbPassword: process.env.DATABASE_PASSWORD || "in14fi04ni20ty20cMl",
		dbConnString: "",

		dbRootUser: process.env.DATABASE_ROOT || "cml_root",
		dbRootPassword: process.env.DATABASE_ROOT_PWD || "camelion_password",
		dbFullString: "",
	};

	config.dbConnString = `${config.dbConnector}://${config.dbUser}:${config.dbPassword}@${config.dbAddress}:${config.dbPort}/`;
	config.dbFullString = `${config.dbConnString}${config.dbName}?authSource=${config.dbAuthSource}&w=1&ssl=false`;

	return config;
};
