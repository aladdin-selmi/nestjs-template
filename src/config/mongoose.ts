import * as mongoose from "mongoose";

mongoose.set("debug", true);

mongoose.set("toJSON", {
	virtuals: true,
	versionKey: false,
	transform: (doc, converted) => {
		delete converted._id;
		delete converted.deleted;
		if (!converted.id) delete converted.id;
	},
});

mongoose.set("toObject", { virtuals: true });

export const addSoftDeletePlugin = (schema) =>
	schema.plugin(require("mongoose-delete"), {
		deletedAt: true,
		indexFields: "all",
		// overrideMethods: "all",
		overrideMethods: ["findOne", "findOneAndUpdate", "update"],
	});

const mongoosePaginate = require("mongoose-paginate-v2");
const customLabels = {
	totalDocs: "count",
	docs: "results",
	// limit: "perPage",
	// page: "currentPage",
	// nextPage: "next",
	// prevPage: "prev",
	// totalPages: "pageCount",
	// pagingCounter: "slNo",
	// meta: "paginator",
};
mongoosePaginate.paginate.options = {
	sort: { createdAt: -1 },
	limit: 10,
	customLabels: customLabels,
};
export const addPaginationPlugin = (schema) => schema.plugin(mongoosePaginate);
