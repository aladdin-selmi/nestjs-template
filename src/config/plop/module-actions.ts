export const moduleActions = (plop, answers) => {
	const moduleName = plop.getHelper("properCase")(answers.moduleInput);
	const moduleFileName = plop.getHelper("dashCase")(answers.moduleInput);
	const modelName = plop.getHelper("properCase")(answers.modelInput);
	const modelFileName = plop.getHelper("dashCase")(answers.modelInput);

	return [
		{
			// Module file
			type: "add",
			path: "src/{{dashCase moduleInput}}/{{dashCase moduleInput}}.module.ts",
			templateFile: "./src/config/plop/module-template/plop.module.hbs",
			data: () => {
				return {
					moduleName,
					moduleFileName,
					modelName,
					modelFileName,
				};
			},
		},
		{
			// Model file
			type: "add",
			path: "src/{{dashCase moduleInput}}/models/{{dashCase modelInput}}.model.ts",
			templateFile: "./src/config/plop/module-template/models/plop.model.hbs",
			data: () => {
				return {
					modelName,
				};
			},
		},
		{
			// Dto Create file
			type: "add",
			path: "src/{{dashCase moduleInput}}/dto/create-{{dashCase modelInput}}.dto.ts",
			templateFile: "./src/config/plop/module-template/dto/create-plop.dto.hbs",
			data: () => {
				return {
					modelName,
				};
			},
		},
		{
			// Dto Update file
			type: "add",
			path: "src/{{dashCase moduleInput}}/dto/update-{{dashCase modelInput}}.dto.ts",
			templateFile: "./src/config/plop/module-template/dto/update-plop.dto.hbs",
			data: () => {
				return {
					modelName,
					modelFileName,
				};
			},
		},
		{
			// Dto Search file
			type: "add",
			path: "src/{{dashCase moduleInput}}/dto/search-{{dashCase modelInput}}.dto.ts",
			templateFile: "./src/config/plop/module-template/dto/search-plop.dto.hbs",
			data: () => {
				return {
					modelName,
				};
			},
		},
		{
			// Service file
			type: "add",
			path: "src/{{dashCase moduleInput}}/{{dashCase moduleInput}}.service.ts",
			templateFile: "./src/config/plop/module-template/plop.service.hbs",
			data: () => {
				return {
					moduleName,
					modelName,
					modelFileName,
					injectedModelName: plop.getHelper("camelCase")(answers.modelInput),
				};
			},
		},
		{
			// Service Spec file
			type: "add",
			path: "src/{{dashCase moduleInput}}/{{dashCase moduleInput}}.service.spec.ts",
			templateFile: "./src/config/plop/module-template/plop.service.spec.hbs",
			data: () => {
				return {
					moduleName,
					moduleFileName,
				};
			},
		},
		{
			// Controller file
			type: "add",
			path: "src/{{dashCase moduleInput}}/{{dashCase moduleInput}}.controller.ts",
			templateFile: "./src/config/plop/module-template/plop.controller.hbs",
			data: () => {
				return {
					moduleName,
					moduleFileName,
					modelName,
					modelFileName,
					injectedModuleName: plop.getHelper("camelCase")(answers.moduleInput),
				};
			},
		},
		{
			// Controller Spec file
			type: "add",
			path: "src/{{dashCase moduleInput}}/{{dashCase moduleInput}}.controller.spec.ts",
			templateFile:
				"./src/config/plop/module-template/plop.controller.spec.hbs",
			data: () => {
				return {
					moduleName,
					moduleFileName,
				};
			},
		},
		{
			// Controller App file
			type: "add",
			path: "src/{{dashCase moduleInput}}/{{dashCase moduleInput}}.app.controller.ts",
			templateFile: "./src/config/plop/module-template/plop.app.controller.hbs",
			data: () => {
				return {
					moduleName,
					moduleFileName,
					modelName,
					modelFileName,
					injectedModuleName: plop.getHelper("camelCase")(answers.moduleInput),
				};
			},
		},
	];
};
