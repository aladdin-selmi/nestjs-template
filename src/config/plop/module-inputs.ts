import { NodePlopAPI } from "plop";

export const moduleInputs = (plop: NodePlopAPI) => {
	return [
		{
			type: "input",
			name: "moduleInput",
			message: "Module name (plural form)",
			validate: function (value) {
				return !!value || "Module name is required";
			},
		},
		{
			type: "input",
			name: "modelInput",
			message: "Model name (singular form)",
			validate: function (value) {
				return !!value || "Model name is required";
			},
		},
	];
};
