import { INestApplication } from "@nestjs/common";
import {
	SwaggerModule,
	DocumentBuilder,
	SwaggerCustomOptions,
} from "@nestjs/swagger";

export function setupSwagger(app: INestApplication) {
	const swaggerConfig = new DocumentBuilder()
		.setTitle("Money Mountain")
		.setDescription("Money Mountain Swagger Docs")
		.setVersion("1.0")
		.addBearerAuth()
		.build();

	const swaggerCustomOptions: SwaggerCustomOptions = {
		swaggerOptions: { persistAuthorization: true, docExpansion: "none" },
	};

	const document = SwaggerModule.createDocument(app, swaggerConfig);
	SwaggerModule.setup("swagger", app, document, swaggerCustomOptions);
}
