import {
	Controller,
	Get,
	Post,
	Body,
	Patch,
	Param,
	Delete,
	Query,
	UsePipes,
	ValidationPipe,
} from "@nestjs/common";
import { CreateCustomerDto } from "./dto/create-customer.dto";
import { UpdateCustomerDto } from "./dto/update-customer.dto";
import { CustomersService } from "./customers.service";
import { ApiBearerAuth, ApiOperation, ApiTags } from "@nestjs/swagger";
import { CustomerSearchDto } from "./dto/customer-search.dto";
import { Roles } from "../auth/roles.decorator";
import { UserRolesEnum } from "../users/models/roles.enum";

@ApiTags("Customers - Admin")
@Controller("admin-api/customers")
@Roles(UserRolesEnum.ADMIN)
@UsePipes(new ValidationPipe({ transform: true, whitelist: true }))
export class CustomersController {
	constructor(private readonly customersService: CustomersService) {}

	@Post()
	@ApiOperation({ summary: "Create" })
	@ApiBearerAuth()
	create(@Body() createCustomerDto: CreateCustomerDto) {
		return this.customersService.create(createCustomerDto);
	}

	@Get()
	@ApiOperation({ summary: "Find all" })
	@ApiBearerAuth()
	findAll(@Query() customerSearchDto: CustomerSearchDto) {
		return this.customersService.findAll(customerSearchDto);
	}

	@Get(":id")
	@ApiOperation({ summary: "FindOne" })
	@ApiBearerAuth()
	findOne(@Param("id") id: string) {
		return this.customersService.findOne(id);
	}

	@Patch(":id")
	@ApiOperation({ summary: "Update" })
	@ApiBearerAuth()
	update(
		@Param("id") id: string,
		@Body() updateCustomerDto: UpdateCustomerDto,
	) {
		return this.customersService.update(id, updateCustomerDto);
	}

	@Delete(":id")
	@ApiOperation({ summary: "Remove" })
	@ApiBearerAuth()
	remove(@Param("id") id: string) {
		return this.customersService.remove(id);
	}
}
