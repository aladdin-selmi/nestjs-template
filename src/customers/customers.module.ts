import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { CustomersService } from "./customers.service";
import { CustomersController } from "./customers.controller";
import { APP_GUARD } from "@nestjs/core";
import { JwtAuthGuard } from "../auth/jwt-auth.guard";
import { CustomerSchema } from "./models/customers.model";
import { addPaginationPlugin, addSoftDeletePlugin } from "../config/mongoose";

@Module({
	controllers: [CustomersController],
	providers: [
		CustomersService,
		{
			provide: APP_GUARD,
			useClass: JwtAuthGuard,
		},
	],
	imports: [
		MongooseModule.forFeatureAsync([
			{
				name: "Customer",
				useFactory: () => {
					addSoftDeletePlugin(CustomerSchema);
					addPaginationPlugin(CustomerSchema);
					return CustomerSchema;
				},
			},
		]),
	],

	exports: [CustomersService],
})
export class CustomersModule {}
