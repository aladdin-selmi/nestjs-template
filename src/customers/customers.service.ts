import {
	Injectable,
	NotFoundException,
	NotAcceptableException,
} from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import * as bcrypt from "bcrypt";
import { Model, Types } from "mongoose";
import { CreateCustomerDto } from "./dto/create-customer.dto";
import { UpdateCustomerDto } from "./dto/update-customer.dto";

import { Customer, CustomerDoc } from "./models/customers.model";
import { QueryManager } from "../query-manager/query-manager";
import { CustomerSearchDto } from "./dto/customer-search.dto";
import { RegisterDto } from "../auth/dto/register.dto";
import { UpdateCustomerProfileDto } from "./dto/update-customer-profile";

@Injectable()
export class CustomersService {
	constructor(
		@InjectModel("Customer") private readonly customerModel: Model<Customer>,
		private readonly queryManager: QueryManager,
	) {}

	async register(registerDto: RegisterDto) {
		await this.validateUniqueCustomer(registerDto);

		const customer: CustomerDoc = new this.customerModel(registerDto);
		await customer.save();

		customer.password = this.cryptPassword(registerDto.password);
		return await customer.save();
	}

	async create(createCustomerDto: CreateCustomerDto) {
		await this.validateUniqueCustomer(createCustomerDto);

		const customer: CustomerDoc = new this.customerModel(createCustomerDto);
		await customer.save();

		customer.password = this.cryptPassword(createCustomerDto.password);
		return await customer.save();
	}

	async findAll(customerSearchDto: CustomerSearchDto) {
		const filter = this.queryManager.buildFilterQuery(
			this.customerModel,
			customerSearchDto,
			["firstName", "lastName", "username"],
		);

		const customers = await this.queryManager.buildPaginationQuery(
			this.customerModel,
			customerSearchDto,
			filter,
		);

		return customers;
	}

	async updateProfile(id: string, updateDto: UpdateCustomerProfileDto) {
		return await this.update(id, updateDto);
	}

	async update(id: string, updateCustomerDto: UpdateCustomerDto) {
		const customer = await this.customerModel.findById(id);
		if (!customer) throw new NotFoundException("Customer not found");

		if (updateCustomerDto.email == customer.email)
			delete updateCustomerDto.email;
		if (updateCustomerDto.username == customer.username)
			delete updateCustomerDto.username;
		await this.validateUniqueCustomer(updateCustomerDto);

		if (updateCustomerDto.hasOwnProperty("password")) {
			updateCustomerDto.password = this.cryptPassword(
				updateCustomerDto.password,
			);
		}

		return await customer.set(updateCustomerDto).save();
	}

	async findOne(id: string) {
		const customer = await this.customerModel.findById(id);
		if (!customer) throw new NotFoundException("Customer not found");
		return customer;
	}

	async findByCredentials(login: string) {
		// TODO: tolowercase login
		const customer = await this.customerModel.findOne({
			$or: [{ username: login }, { email: login }],
		});
		return customer;
	}

	async remove(id: string) {
		// @ts-ignore
		await this.customerModel.deleteById(new Types.ObjectId(id));
		return id;
	}

	// TODO: use Users Service
	private cryptPassword(plainPassword: string): string {
		const salt = bcrypt.genSaltSync(10);
		return bcrypt.hashSync(plainPassword, salt);
	}

	private async validateUniqueCustomer({ email = "", username = "" }) {
		// TODO: compare with lower case
		const $or: any[] = [{ email: email }];
		if (username) $or.push({ username: username });

		//@ts-ignore
		const foundCustomer = await this.customerModel.findOneWithDeleted({ $or });

		if (foundCustomer) {
			const errMessage =
				foundCustomer.email == email
					? "Email already exists"
					: "Username already exists";

			throw new NotAcceptableException(errMessage);
		}
	}
}
