import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNumberString, IsOptional, IsString } from "class-validator";

export class CreateCustomerDto {
	@ApiProperty()
	@IsString()
	firstName: string;

	@ApiProperty()
	@IsString()
	lastName: string;

	@ApiProperty({ required: false })
	@IsNumberString()
	@IsString()
	@IsOptional()
	phone?: string;

	@ApiProperty()
	@IsEmail()
	email: string;

	@ApiProperty()
	@IsString()
	username: string;

	@ApiProperty({ required: false })
	@IsString()
	password: string;
}
