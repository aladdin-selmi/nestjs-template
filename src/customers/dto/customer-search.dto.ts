import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsOptional, IsString } from "class-validator";
import { BaseQueryDto } from "../../query-manager/dto/base-query.dto";

export class CustomerSearchDto extends BaseQueryDto {
	@ApiProperty({ required: false })
	@IsString()
	@IsOptional()
	firstName?: string;

	@ApiProperty({ required: false })
	@IsString()
	@IsOptional()
	lastName?: string;

	@ApiProperty({ required: false })
	@IsOptional()
	phone?: string;

	@ApiProperty({ required: false })
	@IsEmail()
	@IsOptional()
	email?: string;

	@ApiProperty({ required: false })
	@IsString()
	@IsOptional()
	username?: string;
}
