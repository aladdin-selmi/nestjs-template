import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import * as mongoose from "mongoose";

export type CustomerDoc = Customer & mongoose.Document;

@Schema({ timestamps: true })
export class Customer {
	@Prop()
	firstName?: string;

	@Prop()
	lastName?: string;

	@Prop()
	phone?: string;

	// TODO: add unique constraint
	@Prop()
	email: string;

	@Prop()
	username: string;

	@Prop()
	password?: string;
}

export const CustomerSchema = SchemaFactory.createForClass(Customer);

CustomerSchema.set("toJSON", {
	transform: (doc, converted) => {
		delete converted.password;
		delete converted._id;
		if (!converted.id) delete converted.id;
	},
});
