import { ConfigService } from "@nestjs/config";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { setupSwagger } from "./config/swagger";
import helmet from "helmet";
import "./config/mongoose";

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	const configService = app.get(ConfigService);
	const port = configService.get<number>("port");

	setupSwagger(app);
	app.enableCors();
	app.use(helmet());

	await app.listen(port);
	console.log(`Listening on port: ${port}`);
}

bootstrap();
