import { ApiProperty } from "@nestjs/swagger";
import { Transform, Type } from "class-transformer";
import {
	IsBoolean,
	IsIn,
	IsNumber,
	IsOptional,
	IsString,
} from "class-validator";

export class BaseQueryDto {
	@ApiProperty({ required: false })
	@IsString()
	@IsOptional()
	_search?: string;

	@ApiProperty({ required: false })
	@IsBoolean()
	@Transform(({ value }) => value !== "false")
	@IsOptional()
	_pagination?: boolean;

	@ApiProperty({ required: false })
	@IsString()
	@IsOptional()
	_sortBy?: string;

	@ApiProperty({ required: false, enum: [1, -1] })
	@IsIn([1, -1])
	@Transform(({ value }) => parseInt(value))
	@IsOptional()
	_sortOrder?: 1 | -1;

	@ApiProperty({ required: false })
	@IsString()
	@IsOptional()
	_select?: string;

	@ApiProperty({ required: false })
	@IsString()
	@IsOptional()
	_populate?: string;

	@ApiProperty({ required: false })
	@IsNumber()
	@Type(() => Number)
	@IsOptional()
	_offset?: number;

	@ApiProperty({ required: false })
	@IsNumber()
	@Type(() => Number)
	@IsOptional()
	_limit?: number;
}
