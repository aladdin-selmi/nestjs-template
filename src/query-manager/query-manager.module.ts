import { Global, Module } from "@nestjs/common";
import { QueryManager } from "./query-manager";

@Global()
@Module({
	providers: [QueryManager],
	exports: [QueryManager],
})
export class QueryManagerModule {}
