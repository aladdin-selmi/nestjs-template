import { Test, TestingModule } from '@nestjs/testing';
import { QueryManager } from './query-manager';

describe('QueryManager', () => {
  let provider: QueryManager;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [QueryManager],
    }).compile();

    provider = module.get<QueryManager>(QueryManager);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });
});
