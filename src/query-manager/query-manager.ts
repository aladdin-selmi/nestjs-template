import { Injectable } from "@nestjs/common";

const paginationFields = [
	"pagination",
	"sort",
	"select",
	"populate",
	"offset",
	"limit",
];

export enum DeletedFilter {
	ONLY_DELETED,
	WITHOUT_DELETED,
	ALL,
}

@Injectable()
export class QueryManager {
	buildPaginationQuery(
		mongooseModel,
		dto,
		filter = {},
		deletedFilter: DeletedFilter = DeletedFilter.WITHOUT_DELETED,
	) {
		const options = {};

		this.applySortQuery(dto, options);

		paginationFields.forEach((field) => {
			const _field = `_${field}`;
			if (!dto.hasOwnProperty(_field)) return;
			options[field] = dto[_field];
		});

		this.applyDeletedFilter(filter, deletedFilter);

		return mongooseModel.paginate(filter, options);
	}

	private applySortQuery(dto, options) {
		if (!dto._sortBy) return;
		options.sort = {
			[dto._sortBy]: dto._sortOrder,
		};
	}

	private applyDeletedFilter(filter, deletedFilter) {
		if (deletedFilter == DeletedFilter.ALL) return;
		filter["deleted"] =
			deletedFilter == DeletedFilter.WITHOUT_DELETED ? { $ne: true } : true;
	}

	buildFilterQuery(mongooseModel, dto, textFields?) {
		const query = mongooseModel.find();

		this.addSearchQuery(query, textFields, dto);

		for (const attr in dto) {
			query.where(attr, dto[attr]);
		}

		return query.getFilter();
	}

	private addSearchQuery(query, textFields, dto) {
		const search = dto._search;
		if (!textFields || !search) return;

		delete dto._search;

		const _orQuery = textFields.map((field) => ({
			[field]: { $regex: search, $options: "i" },
		}));
		query.or(_orQuery);
	}
}
