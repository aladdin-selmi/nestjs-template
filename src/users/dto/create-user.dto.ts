import { ApiProperty } from "@nestjs/swagger";
import {
	IsEmail,
	IsNotEmpty,
	IsOptional,
	IsString,
	MinLength,
} from "class-validator";
import { UserRolesEnum } from "../models/roles.enum";

export class CreateUserDto {
	@ApiProperty()
	@MinLength(3)
	@IsString()
	@IsNotEmpty()
	firstName: string;

	@ApiProperty()
	@MinLength(3)
	@IsString()
	@IsNotEmpty()
	lastName: string;

	@ApiProperty()
	@IsEmail()
	@IsNotEmpty()
	email: string;

	@ApiProperty()
	@MinLength(3)
	@IsString()
	@IsNotEmpty()
	username: string;

	@ApiProperty({ required: false })
	@MinLength(3)
	@IsString()
	@IsOptional()
	password?: string;

	roles = [UserRolesEnum.ADMIN];
}
