import { ApiProperty } from "@nestjs/swagger";
import { BaseQueryDto } from "../../query-manager/dto/base-query.dto";

export class SearchUserDto extends BaseQueryDto {
	@ApiProperty({ required: false })
	firstName?: string;

	@ApiProperty({ required: false })
	lastName?: string;

	@ApiProperty({ required: false })
	email?: string;

	@ApiProperty({ required: false })
	username?: string;
}
