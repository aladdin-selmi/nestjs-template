import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import * as mongoose from "mongoose";
import { UserRolesEnum } from "./roles.enum";

export type UserDoc = User & mongoose.Document;

@Schema({ timestamps: true })
export class User {
	@Prop({ required: false })
	firstName?: string;

	@Prop({ required: false })
	lastName?: string;

	// TODO: add unique constraint
	@Prop()
	email: string;

	@Prop()
	username: string;

	@Prop()
	password?: string;

	@Prop({ type: [{ type: String, enum: UserRolesEnum }] })
	roles: UserRolesEnum[];
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.set("toJSON", {
	transform: (doc, converted) => {
		delete converted.password;
		delete converted._id;
		if (!converted.id) delete converted.id;
	},
});
