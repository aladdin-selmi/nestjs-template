import {
	Controller,
	Get,
	Post,
	Body,
	Param,
	Delete,
	ValidationPipe,
	UsePipes,
	Query,
} from "@nestjs/common";
import { UsersService } from "./users.service";
import { CreateUserDto } from "./dto/create-user.dto";
import { ApiBearerAuth, ApiOperation, ApiTags } from "@nestjs/swagger";
import { SearchUserDto } from "./dto/search-user.dto";
import { Roles } from "../auth/roles.decorator";
import { UserRolesEnum } from "./models/roles.enum";

@Controller("admin-api/users")
@ApiTags("Users - Admin")
@Roles(UserRolesEnum.ADMIN)
@UsePipes(new ValidationPipe({ transform: true, whitelist: true }))
export class UsersController {
	constructor(private readonly usersService: UsersService) {}

	@Post()
	@ApiOperation({ summary: "Create" })
	@ApiBearerAuth()
	create(@Body() createUserDto: CreateUserDto) {
		return this.usersService.create(createUserDto);
	}

	@Get()
	@ApiOperation({ summary: "Find all" })
	@ApiBearerAuth()
	findAll(@Query() searchUserDto: SearchUserDto) {
		return this.usersService.findAll(searchUserDto);
	}

	@Get(":id")
	@ApiOperation({ summary: "FindOne" })
	@ApiBearerAuth()
	findOne(@Param("id") id: string) {
		return this.usersService.findOne(id);
	}

	@Delete(":id")
	@ApiOperation({ summary: "Remove" })
	@ApiBearerAuth()
	remove(@Param("id") id: string) {
		return this.usersService.remove(id);
	}
}
