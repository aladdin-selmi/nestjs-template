import { Module } from "@nestjs/common";
import { UsersService } from "./users.service";
import { UsersController } from "./users.controller";
import { MongooseModule } from "@nestjs/mongoose";
import { UserSchema } from "./models/users.model";
// import { APP_GUARD } from "@nestjs/core";
// import { JwtAuthGuard } from "../auth/jwt-auth.guard";
import { addPaginationPlugin, addSoftDeletePlugin } from "../config/mongoose";

@Module({
	controllers: [UsersController],
	providers: [
		UsersService,
		// {
		// 	provide: APP_GUARD,
		// 	useClass: JwtAuthGuard,
		// },
	],
	imports: [
		MongooseModule.forFeatureAsync([
			{
				name: "User",
				useFactory: () => {
					addSoftDeletePlugin(UserSchema);
					addPaginationPlugin(UserSchema);
					return UserSchema;
				},
			},
		]),
	],
	exports: [UsersService],
})
export class UsersModule {}
