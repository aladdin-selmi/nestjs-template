import {
	Injectable,
	NotFoundException,
	NotAcceptableException,
} from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import * as bcrypt from "bcrypt";
import { Model, Types } from "mongoose";
import { QueryManager } from "../query-manager/query-manager";
import { CreateUserDto } from "./dto/create-user.dto";
import { SearchUserDto } from "./dto/search-user.dto";
import { UpdateUserProfileDto } from "./dto/update-user-profile.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { UserRolesEnum } from "./models/roles.enum";
import { User, UserDoc } from "./models/users.model";

@Injectable()
export class UsersService {
	constructor(
		@InjectModel("User") private readonly userModel: Model<User>,
		private readonly queryManager: QueryManager,
	) {}

	async create(createUserDto: CreateUserDto) {
		await this.validateUniqueUser(createUserDto);

		createUserDto.roles = [UserRolesEnum.ADMIN];

		createUserDto.password = this.cryptPassword(createUserDto.password);

		const newUser: UserDoc = new this.userModel(createUserDto);

		return await newUser.save();
	}

	async findAll(searchUserDto: SearchUserDto) {
		const filter = this.queryManager.buildFilterQuery(
			this.userModel,
			searchUserDto,
			["firstName", "lastName", "username"],
		);

		return await this.queryManager.buildPaginationQuery(
			this.userModel,
			searchUserDto,
			filter,
		);
	}

	async findOne(userId: string) {
		const user = await this.userModel.findById(userId);
		if (!user) throw new NotFoundException("User not found");
		return user;
	}

	async findByCredentials(login: string) {
		// TODO: tolowercase login
		const user = await this.userModel.findOne({
			$or: [{ username: login }, { email: login }],
		});

		return user;
	}

	async updateProfile(id: string, updateDto: UpdateUserProfileDto) {
		return await this.update(id, updateDto);
	}

	async update(id: string, updateUserDto: UpdateUserDto) {
		const user = await this.userModel.findById(id);
		if (!user) throw new NotFoundException("User not found");

		if (updateUserDto.email == user.email) delete updateUserDto.email;
		if (updateUserDto.username == user.username) delete updateUserDto.username;
		await this.validateUniqueUser(updateUserDto);

		if (updateUserDto.hasOwnProperty("password")) {
			updateUserDto.password = this.cryptPassword(updateUserDto.password);
		}

		return await user.set(updateUserDto).save();
	}

	async remove(id: string) {
		// @ts-ignore
		await this.userModel.deleteById(new Types.ObjectId(id));
		return id;
	}

	private cryptPassword(plainPassword: string): string {
		const salt = bcrypt.genSaltSync(10);
		return bcrypt.hashSync(plainPassword, salt);
	}

	private async validateUniqueUser(userDto: CreateUserDto | UpdateUserDto) {
		// TODO: compare with lower case
		//@ts-ignore
		const foundUser = await this.userModel.findOneWithDeleted({
			$or: [{ username: userDto.username }, { email: userDto.email }],
		});

		if (foundUser) {
			const errMessage =
				foundUser.email == userDto.email
					? "Email already exists"
					: "Username already exists";

			throw new NotAcceptableException(errMessage);
		}
	}
}
